﻿using UnityEngine;
using System.Collections;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;

public class FireBat1 : MonoBehaviour
{

    public GameObject Batedor1;

    public GameObject colA;
    public GameObject colB;
    public GameObject colC;
    public GameObject colD;

    float qtx;
    float qtz;

    float menorX;
    float menorZ;
    float maiorX;
    float maiorZ;


    // Use this for initialization
    void Start()
    {

        menorX = colA.transform.localPosition.x;
        maiorX = colC.transform.localPosition.x;
        menorZ = colD.transform.localPosition.z;
        maiorZ = colB.transform.localPosition.z;


        Debug.Log("X " + (maiorX - menorX));
        Debug.Log("Z " + (maiorZ - menorZ));

        qtx = (maiorX - menorX) / 1907;
        qtz = (maiorZ - menorZ) / 1070;

        Debug.Log("valor x " + qtx);
        Debug.Log("valor z " + qtz);


        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://arhockey-3af80.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

        FirebaseDatabase.DefaultInstance.GetReference("positions/player1").ValueChanged += (object sender, ValueChangedEventArgs args) =>
        {


            DataSnapshot snapshot = args.Snapshot;
            // Do something with snapshot...
            string aux;
            aux = (string)snapshot.Child("x").Value.ToString();
            float xdl = float.Parse(aux);

            aux = (string)snapshot.Child("y").Value.ToString();
            float zdl = float.Parse(aux);

            Batedor1.transform.localPosition = new Vector3((menorX + (xdl * qtx)) * 1, 0, (menorZ + (zdl * qtz)) * -1);

        };

    }
}


