﻿using UnityEngine;
using System.Collections;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;

public class FireTest : MonoBehaviour
{

    public GameObject textData;
    public GameObject Batedor1;
    public GameObject Batedor2;
    public GameObject Puck;

    public GameObject colA;
    public GameObject colB;
    public GameObject colC;
    public GameObject colD;

    float qtx;
    //2.36f / 1907;
    //0.0017289377289377f;
    float qtz;
    //1.24f / 1070;
    //0.0016166883963494f;

    float menorX;
    float menorZ;
    float maiorX;
    float maiorZ;

    public void Init()
    {
        menorX = colA.transform.localPosition.x;
        maiorX = colC.transform.localPosition.x;
        menorZ = colD.transform.localPosition.z;
        maiorZ = colB.transform.localPosition.z;
        /*
         * menorX = colA.transform.position.x / 10;
        maiorX = colC.transform.position.x / 10;
        menorZ = colD.transform.position.z / 10;
        maiorZ = colB.transform.position.z / 10;
         */
    }

    // Use this for initialization
    void Start()
    {

        Init();


        Debug.Log("X " + (maiorX - menorX));
        Debug.Log("Z " + (maiorZ - menorZ));

        qtx = (maiorX - menorX) / 1907;
        qtz = (maiorZ - menorZ) / 1070;

        Debug.Log("valor x " + qtx);
        Debug.Log("valor z " + qtz);


        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://arhockey-3af80.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

        FirebaseDatabase.DefaultInstance.GetReference("positions").ValueChanged += (object sender, ValueChangedEventArgs args) =>
        {


            DataSnapshot snapshot = args.Snapshot;
            // Do something with snapshot...
            string aux;
            float xdl, zdl;

            
            aux = (string)snapshot.Child("player1/x").Value.ToString();
            xdl = float.Parse(aux);

            aux = (string)snapshot.Child("player1/y").Value.ToString();
            zdl = float.Parse(aux);

            Batedor1.transform.localPosition = new Vector3((menorX + (xdl * qtx)) * 1, 0, (menorZ + (zdl * qtz)) * -1);
            

            aux = (string)snapshot.Child("player2/x").Value.ToString();
            xdl = float.Parse(aux);

            aux = (string)snapshot.Child("player2/y").Value.ToString();
            zdl = float.Parse(aux);

            Batedor2.transform.localPosition = new Vector3((menorX + (xdl * qtx)) * 1, 0, (menorZ + (zdl * qtz)) * -1);
            

            aux = (string)snapshot.Child("puck/x").Value.ToString();
            xdl = float.Parse(aux);

            aux = (string)snapshot.Child("puck/y").Value.ToString();
            zdl = float.Parse(aux);

            Puck.transform.localPosition = new Vector3((menorX + (xdl * qtx)) * 1, 0, (menorZ + (zdl * qtz)) * -1);
            
        };

    }

    // Update is called once per frame
    void Update()
    {


    }



}


