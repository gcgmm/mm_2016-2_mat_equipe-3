var ballX = 0,
	ballY = 0;

var acceleration = 1.18;
var deacceleration = 0.996;

function Game() {
    var canvas = document.getElementById("game");
    this.width = canvas.width;
    this.height = canvas.height;
    this.context = canvas.getContext("2d");
    this.context.fillStyle = "black";
    
    this.p1 = new Paddle(5, 0, "blue");
    this.p1.y = this.height/2 - this.p1.height/2;
    this.display1 = new Display(this.width/4, 25);
    this.p2 = new Paddle(this.width - 5 - 2, 0, "red");
    this.p2.y = this.height/2 - this.p2.height/2;
    this.display2 = new Display(this.width*3/4, 25);
    
    this.ball = new Ball();
    this.ball.x = this.width/2;
    this.ball.y = this.height/2;
    this.ball.vy = Math.floor(Math.random()*12);
	if(Math.abs(this.ball.vy) < 1) this.ball.vy += 3;
    this.ball.vx = 16;
}

Game.prototype.draw = function()
{
    this.context.clearRect(0, 0, this.width, this.height);
    this.context.fillRect(this.width/2, 0, 2, this.height);
    
    this.ball.draw(this.context);
    
    this.p1.draw(this.context);
    this.p2.draw(this.context);
    this.display1.draw(this.context);
    this.display2.draw(this.context);
};
 
Game.prototype.update = function() 
{
    if (this.paused)
        return;
	
	ballX = this.ball.x;
	ballY = this.ball.y;
	
    this.ball.update();
    this.display1.value = this.p1.score;
    this.display2.value = this.p2.score;
 
    this.p1.x = p1x;
	this.p1.y = p1y;
	this.p2.x = p2x;
	this.p2.y = p2y;
	
	if(this.ball.vx > 10){
		this.ball.vx *= deacceleration;
		this.ball.vy *= deacceleration;
	}
 
    if (this.ball.vx > 0) {
		//se a bola vai pra direita
		
		var deltax = this.p2.x + (raio*2);
		deltax -= this.ball.x + (raio*3);
		
		var deltay = this.p2.y + (raio * 2);
		deltay -= this.ball.y + (raio * 3);
		
		var dist = deltax * deltax + deltay * deltay;
		if(dist < ((raio * 3) * (raio * 3))){
			//colisao
			this.ball.vx = -this.ball.vx;
			if(this.p2.y < this.ball.y){
				this.ball.vy = 10;
			} else {
				this.ball.vy = -10;
			}
			this.ball.vx *= acceleration;
			this.ball.vy *= acceleration;
		}
    } else {
		var deltax = this.p1.x + (raio*2);
		deltax -= this.ball.x + (raio*3);
		
		var deltay = this.p1.y + (raio * 2);
		deltay -= this.ball.y + (raio * 3);
		
		var dist = deltax * deltax + deltay * deltay;
		if(dist < ((raio * 3) * (raio * 3))){
			//colisao
			this.ball.vx = -this.ball.vx;
			if(this.p1.y < this.ball.y){
				this.ball.vy = 10;
			} else {
				this.ball.vy = -10;
			}
			this.ball.vx *= acceleration;
			this.ball.vy *= acceleration;
		}
    }
 
    // Top and bottom collision
    if ((this.ball.vy < 0 && this.ball.y < 0) ||
            (this.ball.vy > 0 && this.ball.y + this.ball.height > this.height)) {
        this.ball.vy = -this.ball.vy;
		this.ball.vx *= acceleration;
		this.ball.vy *= acceleration;
    }
    
    if (this.ball.x >= this.width){
		if(this.ball.y < alturaTela / 3 * 2 && this.ball.y > alturaTela / 3){
			this.score(this.p1);
			firebase.database().ref('positions/player1').update({
				score: this.p1.score
			});
		} else {
			this.ball.vx *= -1;
		}
	} else if (this.ball.x + this.ball.width <= 0){
		if(this.ball.y < alturaTela / 3 * 2 && this.ball.y > alturaTela / 3){
			this.score(this.p2);
			firebase.database().ref('positions/player2').update({
				score: this.p1.score
			});
		} else {
			this.ball.vx *= -1;
		}
	}
        
};

Game.prototype.score = function(p)
{
    // player scores
    p.score++;
    var player = p == this.p1 ? 0 : 1;
 
    // set ball position
    this.ball.x = this.width/2;
    this.ball.y = p.y + p.height/2;
 
    // set ball velocity
    this.ball.vy = Math.floor(Math.random()*12);
	if(Math.abs(this.ball.vy) < 1) this.ball.vy += 3;
    this.ball.vx = 16;
    if (player == 1)
        this.ball.vx *= -1;
};


//PADDLE
function Paddle(x,y, color) {
    this.x = x;
    this.y = y;
    this.width = 12;
    this.height = 28;
    this.score = 0;
	this.color = color;
}

Paddle.prototype.draw = function(p)
{
	p.fillStyle = this.color;
   	
	p.beginPath();
	p.arc(this.x+this.width/2,this.y+this.height/2,raio*2,0,2*Math.PI);
	p.fill();
	
	p.fillStyle = "black";
};


//BALL
function Ball() {
    this.x = 0;
    this.y = 0;
    this.vx = 0;
    this.vy = 0;
    this.width = 32;
    this.height = 32;
}
 
Ball.prototype.update = function()
{
    this.x += this.vx;
    this.y += this.vy;
};
 
Ball.prototype.draw = function(p)
{	
	p.beginPath();
	p.arc(this.x+this.width/2,this.y+this.height/2,raio*3,0,2*Math.PI);
	p.fill();
};


//DISPLAY
function Display(x, y) {
    this.x = x;
    this.y = y;
    this.value = 0;
}
 
Display.prototype.draw = function(p)
{
    p.fillText(this.value, this.x, this.y);
};


// Initialize our game instance
var game = new Game();
 
function MainLoop() {
    game.update();
    game.draw();
    // Call the main loop again at a frame rate of 30fps
    setTimeout(MainLoop, 33.3333);
}
 
// Start the game execution
MainLoop();

//Update puck's position
window.setInterval(function (){
	firebase.database().ref('positions/puck').set({
		x: ballX,
		y: ballY
	});
}, 100);